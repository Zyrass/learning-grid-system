// Selection des classes issue des textarea (DOM)
const codeCSS = document.querySelectorAll(".codeCSS");
const codeHTML = document.querySelectorAll(".codeHTML");
const codeSHELL = document.querySelectorAll(".codeSHELL");

// Tableaux des tableaux textareas
const codeLists = [codeCSS, codeHTML, codeSHELL];

// Constantes pour les thèmes à utilisé dans l'initialisation de la fonction
const ambiance = "ambiance"; // bg: noir font: vert comments: gris foncé

// Constante des thèmes modifié par mes soins
const zyrass = "ambiance_zyrass";

// Object qui sera utilisé dans la fonction ci-dessous pour éviter de se répéter
const objectConfiguration = {
  lineNumbers: true, // Permet d'afficher les numéros de ligne
  matchBrackets: true, // Permet de cibler un tag ouvrant et fermant
  autoCloseBrackets: true, // Ferme automatiquement les accolades
  lineWrapping: false, // Permet ici de ne pas revenir à la ligne
  scrollbarStyle: "overlay", // Change le style de la scrollbar
};

/**
 * La méthode myObject, permet de configurer CodeMirror plus facilement.
 * Une configuration existe qui est pré-utilisé mais on a la possibilité
 * d'écraser certaines valeurs.
 *
 * @param {string} mode             // Définir directement le type retourné
 * @param {Integer} tabSize         // Permet de définir la tabulation
 * @param {string} theme            // Spécifier le thème utilisé
 * @param {boolean} readOnly        // Permet de spécifier un contenu
 * @param {boolean} lineWrapping    // Permet de forcer le retour à la ligne
 */
const myObject = (
  mode,
  tabSize = 4,
  theme = zyrass,
  readOnly = true,
  lineWrapping = false
) => {
  const data = objectConfiguration;
  data.mode = mode;
  data.tabSize = tabSize;
  data.theme = theme;
  data.readOnly = readOnly;
  data.lineWrapping = lineWrapping;
  return data;
};

// Boucle sur toutes les listes disponibles
codeLists.forEach((retourCodeList) => {
  // Boucle sur chaque liste indépendamment des autres
  // Puis on applique un mode spécifique selon le langage utilisé
  for (let currentCode of retourCodeList) {
    if (currentCode.classList.contains("codeCSS")) {
      CodeMirror.fromTextArea(currentCode, myObject("css"));
    } else if (currentCode.classList.contains("codeHTML")) {
      CodeMirror.fromTextArea(
        currentCode,
        myObject("text/html", 4, zyrass, true, true)
      );
    } else if (currentCode.classList.contains("codeSHELL")) {
      CodeMirror.fromTextArea(currentCode, myObject("application/x-sh"));
    }
  }
});
