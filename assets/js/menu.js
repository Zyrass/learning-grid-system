const listes = document.querySelectorAll('ol[type="1"] > li');
const listeBase = document.querySelector("#baseMenu + ol");
const listePositionnement = document.querySelector("#positionnementMenu + ol");
const listeZone = document.querySelector("#zoneMenu + ol");
const listeGutters = document.querySelector("#guttersMenu + ol");

console.log(listes);
// console.log(listeVisibled);

const hide = (select, text) => {
  document.querySelector(select).setAttribute("class", "");
  document.querySelector(select).innerHTML = text;
};

listes.forEach((currentElement) => {
  currentElement.addEventListener("click", () => {
    currentElement.classList.toggle("visibled");

    if (currentElement.classList.contains("visibled")) {
      /* VISIBLE */

      if (currentElement.getAttribute("id") === "baseMenu") {
        currentElement.innerHTML = "1 - Les bases <span>⇣</span>";
        listeBase.style.display = "block";
        hide('ol[type="1"] > li#positionnementMenu', "2 - Le positionnement <span>⇠</span>");
        hide('ol[type="1"] > li#zoneMenu', "3 - Les zones <span>⇠</span>");
        hide('ol[type="1"] > li#guttersMenu', "4 - Les gouttières <span>⇠</span>");
        listePositionnement.style.display = "none";
        listeZone.style.display = "none";
        listeGutters.style.display = "none";
      }

      if (currentElement.getAttribute("id") === "positionnementMenu") {
        currentElement.innerHTML = "2 - Le positionnement <span>⇣</span>";
        listePositionnement.style.display = "block";
        hide('ol[type="1"] > li#baseMenu', "1 - Les bases <span>⇠</span>");
        hide('ol[type="1"] > li#zoneMenu', "3 - Les zones <span>⇠</span>");
        hide('ol[type="1"] > li#guttersMenu', "4 - Les gouttières <span>⇠</span>");
        listeBase.style.display = "none";
        listeZone.style.display = "none";
        listeGutters.style.display = "none";
      }

      if (currentElement.getAttribute("id") === "zoneMenu") {
        currentElement.innerHTML = "3 - Les zones <span>⇣</span>";
        listeZone.style.display = "block";
        hide('ol[type="1"] > li#baseMenu', "1 - Les bases <span>⇠</span>");
        hide('ol[type="1"] > li#positionnementMenu', "2 - Le positionnement <span>⇠</span>");
        hide('ol[type="1"] > li#guttersMenu', "4 - Les gouttières <span>⇠</span>");
        listeBase.style.display = "none";
        listePositionnement.style.display = "none";
        listeGutters.style.display = "none";
      }

      if (currentElement.getAttribute("id") === "guttersMenu") {
        currentElement.innerHTML = "4 - Les gouttières <span>⇣</span>";
        listeGutters.style.display = "block";
        hide('ol[type="1"] > li#baseMenu', "1 - Les bases <span>⇠</span>");
        hide('ol[type="1"] > li#positionnementMenu', "2 - Le positionnement <span>⇠</span>");
        hide('ol[type="1"] > li#zoneMenu', "3 - Les zones <span>⇠</span>");
        listeBase.style.display = "none";
        listePositionnement.style.display = "none";
        listeZone.style.display = "none";
      }
    } else {
      /* NON VISIBLE */

      if (currentElement.getAttribute("id") === "baseMenu") {
        currentElement.innerHTML = "1 - Les bases <span>⇠</span>";
        listeBase.style.display = "none";
      }

      if (currentElement.getAttribute("id") === "positionnementMenu") {
        currentElement.innerHTML = "2 - Le positionnement <span>⇠</span>";
        listePositionnement.style.display = "none";
      }

      if (currentElement.getAttribute("id") === "zoneMenu") {
        currentElement.innerHTML = "3 - Les zones <span>⇠</span>";
        listeZone.style.display = "none";
      }

      if (currentElement.getAttribute("id") === "guttersMenu") {
        currentElement.innerHTML = "4 - Les gouttières <span>⇠</span>";
        listeGutters.style.display = "none";
      }
    }
  });
});
